class Validator:
    @staticmethod
    def check_move(move, board):
        if move.isdigit() and 0 <= int(move) < 9:
            if Validator.check_board(move, board):
                return True
        return False

    @staticmethod
    def check_board(move, board):
        if board[int(move)] == ' ':
            return True
        return False
