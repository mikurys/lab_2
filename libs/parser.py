#!/usr/bin/env python

"""parser.py: Parse XML files in tuples

__author__ = "Ryszard Mikulec" """

import sys
import xml.etree.ElementTree as ET


class Parser():
    def __init__(self, XMLfile):
        """ Init objest with XML file path"""
        try:
            self.root = ET.parse(XMLfile).getroot()
        except FileNotFoundError as error:
            sys.exit(error.strerror)

    def parseFile(self):
        """Return tuple with data from parsed file"""
        for child in self.root:
            elementInfo = []
            elementInfo.append(child.attrib['id'])
            for element in child:
                elementInfo.append(element.text)
            yield tuple(elementInfo)


if __name__ == '__main__':
    books = Parser('Books.xml').parseFile()
    for i in range(12):
        print(next(books))

 #   for book in books:
#        print(book)