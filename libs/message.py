import pickle


class Message:
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

    def __eq__(self, other):
        if self.msg == other.msg:
            return True
        return False

    def to_bytes(self):
        return pickle.dumps(self)


class StartMessage(Message):
    pass


class EndMessage(Message):
    pass


class BoardMessage(Message):
    pass


class MoveMessage(Message):
    pass


class MessageCreator:
    @staticmethod
    def create_message(msg, type=''):
        if isinstance(msg, bytes):
            return pickle.loads(msg)
        elif isinstance(msg, str):
            if type == "EndMessage":
                return EndMessage(msg)
            elif type == "BoardMessage":
                return BoardMessage(msg)
            elif type == "StartMessage":
                return StartMessage(msg)
            elif type == "MoveMessage":
                return MoveMessage(msg)
