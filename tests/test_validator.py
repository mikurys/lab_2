from unittest import TestCase
from libs.validator import Validator as validator


class TestValidator(TestCase):
  def test_checkMove_good_move(self):
    val = validator()
    board = "  xo   x "
    good_move = '1'
    self.assertEqual(val.check_move(good_move, board), True)

  def test_checkMove_not_digit(self):
    val = validator()
    board = "  xo   x "
    move_not_digit = 'g'
    self.assertEqual(val.check_move(move_not_digit, board), False)

  def test_checkMove_move_not_in_range(self):
    val = validator()
    board = "  xo   x "
    move_not_in_range = '11'
    self.assertEqual(val.check_move(move_not_in_range, board), False)

  def test_checkMove_incorect_position(self):
    val = validator()
    board = "  xo   x "
    incorect_position = '2'
    self.assertEqual(val.check_move(incorect_position, board), False)

  def test_checkBoard_correct_positions(self):
    val = validator()
    board = " oxo  xx "
    corect_positions = (0, 4, 5, 8)
    for position in corect_positions:
        self.assertEqual(val.check_board(position, board), True)

  def test_checkBoard_incorrect_positions(self):
    val = validator()
    board = " oxo  xx "
    incorect_positions = (1, 2, 3, 6, 7)
    for position in incorect_positions:
        self.assertEqual(val.check_board(position, board), False)
