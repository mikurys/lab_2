from unittest import TestCase
from libs.parser import Parser


class TestParser(TestCase):
    def test_parseFile_should_return_a_tuple(self):
        books = Parser('test.xml').parseFile()
        expected_tuple1 = ('tb01', 'test author 1', "test title 1", 'test', '9.99', '2015-10-01', 'Test description 1.')
        expected_tuple2 = ('tb02', 'test author 2', "test title 2", 'test', '5.55', '2015-12-16', 'Test description 2.')
        self.assertTupleEqual(next(books), expected_tuple1)
        self.assertTupleEqual(next(books), expected_tuple2)

    def test_parseFilel_should_rise_exception_when_amount_of_item_is_exceeded(self):
        books = Parser('test.xml').parseFile()
        for i in range(2):
            next(books)
        self.assertRaises(StopIteration, next, books)

    def test_parseFile_should_display_massage_and_exit_when_file_doesnt_not_exist(self):
        expected_massage = "No such file or directory"
        self.assertRaisesRegex(SystemExit, expected_massage, Parser, 'tests.xml')

if __name__ == '__main__':
    TestCase.main()