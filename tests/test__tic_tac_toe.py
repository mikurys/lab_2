from unittest import TestCase
from server.tic_tac_toe import TicTacToe
from libs.message import Message as msg


class TestTicTacToe(TestCase):
  def test_new_game(self):
    game = TicTacToe()
    message = msg('         ', 'board')
    self.assertEqual(game.new_game(), message)

  def test_is_not_over_is_win(self):
    game = TicTacToe()
    win = ['x', 'x', 'x', 'o', 'o', ' ', ' ', ' ', ' ']
    game.board = win
    self.assertEqual(game.is_not_over(), False)

  def test_is_not_over_is_draw(self):
    game = TicTacToe()
    draw = ['x', 'o', 'o', 'o', 'x', 'x', 'x', 'x', 'o']
    game.board = draw
    self.assertEqual(game.is_not_over(), False)

  def test_is_not_over(self):
    game = TicTacToe()
    not_over = ['x', 'o', 'x', 'o', 'o', 'x', 'x', ' ', ' ']
    game.board = not_over
    self.assertEqual(game.is_not_over(), True)

  def test_get_end_board(self):
    game = TicTacToe()
    game.board = 'xxxoo    '
    message = msg(game.board, 'end')
    self.assertEqual(game.get_end_board(), message)

  def test_next_move(self):
    self.fail()
