from unittest import TestCase
from more_or_less import MoreOrLess
from message import *


class TestMoreOrLess(TestCase):
  def test_next_move_more_message_return(self):
    game = MoreOrLess()
    game.number = 20
    move = 15
    self.assertEqual(game.next_move(move), BoardMessage('More'))

  def test_next_move_less_message_return(self):
    game = MoreOrLess()
    game.number = 20
    move = 25
    self.assertEqual(game.next_move(move), BoardMessage('Less'))

  def test_next_move_end_message_return(self):
    game = MoreOrLess()
    game.number = 20
    move = 20
    self.assertEqual(game.next_move(move), EndMessage('You win'))
