import socket
from tic_tac_toe import TicTacToe
from more_or_less import MoreOrLess
from message import *
from parser import Parser


class Server:
    def __init__(self, config_file='../config/ServerConfig.xml'):
        config = next(Parser(config_file).parseFile())
        self.data_size = int(config[3])
        self._create_tcp_ip_socket()
        self.game = None
        self._bind_socket_to_the_port((config[1], int(config[2])))

    def handle_connection(self):
        self.sock.listen(10)
        connection, client_address = self.sock.accept()
        while True:
            message = connection.recv(self.data_size)
            if message:
                print(MessageCreator.create_message(message))
                message = self.handle_data(MessageCreator.create_message(message))
                connection.send(message.to_bytes())
        connection.close()

    def handle_data(self, message):
        if isinstance(message, StartMessage):
            if message == StartMessage('TicTacToe'):
                self.game = TicTacToe()
            if message == StartMessage('MoreOrLess'):
                self.game = MoreOrLess()
            return self.game.new_game()
        elif isinstance(message, BoardMessage):
            return self.game.get_end_board()
        elif isinstance(message, MoveMessage):
            self.game.next_move(message.__str__())
            return self.game.next_move(message.__str__())

    def _create_tcp_ip_socket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _bind_socket_to_the_port(self, server_address):
        print('bind to %s port %s' % server_address)
        self.sock.bind(server_address)


if __name__ == "__main__":
    server = Server()
    server.handle_connection()
