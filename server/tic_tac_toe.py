from computer import Computer
from message import *
from game import Game


class TicTacToe(Game):
    def __init__(self, multiplayer=False, computer_level=1):
        self.board = []
        self.moves = 0
        if not multiplayer:
            self.player = Computer(computer_level)

    def new_game(self):
        self.board = [' ' for x in range(9)]
        self.moves = 0
        return BoardMessage((9*'{}').format(*self.board))

    def load_game(self):
        pass

    def is_not_over(self):
        wins = ((0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 3, 7), (2, 5, 8), (2, 5, 8), (0, 4, 8), (2, 4, 6))
        for win in wins:
            if self.board[win[0]] == self.board[win[1]]and self.board[win[0]] == self.board[win[2]] and self.board[win[1]] != ' ':
                return False
        if ' ' in self.board:
            return True
        return False

    def get_end_board(self):
        return EndMessage(self.board)

    def next_move(self, move):
        if self.is_not_over():
            if self.moves % 2:
                self.board[self.player.make_move(self.board)] = 'o'
            else:
                self.board[int(move)] = 'x'
            self.moves += 1
            return BoardMessage((9*'{}').format(*self.board))
        elif self.moves == 8:
            return EndMessage("Game Over. Drow")
        elif self.moves % 2:
            return EndMessage("Game Over. Player win")
        else:
            return EndMessage("Game Over. Computer win")

    def play(self):
        while self.is_not_over():
            if self.moves % 2:
                self.board[self.player.make_move(self.board)] = 'o'
            else:
                self.board[self.get_move()] = 'x'
            self.moves += 1
        if self.moves == 8:
            return "Drow"
        elif self.moves % 2:
            return "Player win"
        else:
            return "Computer win"

    def get_move(self):
        move = int(input(self.board))
        return move

if __name__ == "__main__":
    game = TicTacToe()
    game.new_game()
    print(game.play())
