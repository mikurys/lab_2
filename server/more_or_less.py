from message import *
from game import Game
import random


class MoreOrLess(Game):
    def __init__(self, max=100):
        self.max = max
        self.number = None
        self.moves = 0

    def new_game(self):
        self.number = random.randint(0, self.max)
        return BoardMessage('What is my number(0-100)?')

    def load_game(self):
        pass

    def get_end_board(self):
        return EndMessage(self.number)

    def next_move(self, move):
        if int(move) < self.number:
            return BoardMessage('More')
        elif int(move) > self.number:
            return BoardMessage('Less')
        elif int(move) == self.number:
            return EndMessage('You win')
