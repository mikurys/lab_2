import random


class Computer:
    def __init__(self, level=1):
        self.level = level

    def monkey_level(self, board):
        moves = [i for i in range(9) if board[i] == ' ']
        return random.choice(moves)

    def make_move(self, board):
        if self.level == 1:
            move = self.monkey_level(board)
            return move
