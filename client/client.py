#!/usr/bin/python

import socket
import sys
from parser import Parser
from message import *
from board import Board
from validator import Validator


class Client:
    def __init__(self, config_file='../config/ServerConfig.xml'):
        config = next(Parser(config_file).parseFile())
        self.server_address = (config[1], int(config[2]))
        self.data_size = int(config[3])
        self._create_tcp_ip_socket()
        self.connect_to_server()

    def send_msg(self, message):
        self.sock.send(message.to_bytes())
        return self.sock.recv(self.data_size)

    def _create_tcp_ip_socket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect_to_server(self):
        print('connecting to %s port %s' % self.server_address)
        try:
            self.sock.connect(self.server_address)
        except ConnectionRefusedError:
            sys.exit('Connection refused! Check server is running or/and config file.')


if __name__ == "__main__":
    client = Client()
    while True:
        game = input("Do you want play (T)ic Tac Toe or (M)ore Or Less? If you want (q)uit, write q\n")
        if game.lower()[0] == 't':
            board = Board()
            board.display(MessageCreator.create_message(client.send_msg(StartMessage('TicTacToe'))).__str__())
            move = input()
            while not Validator.check_move(move, list(' '*9)):
                move = input()
            response = MessageCreator.create_message(client.send_msg(MoveMessage(move)))
            while not isinstance(response, EndMessage):
                board.display(response.__str__())
                move = input()
                while not Validator.check_move(move, list(str(response))):
                    move = input()
                response = MessageCreator.create_message(client.send_msg(MoveMessage(move)))
            board.display(MessageCreator.create_message(client.send_msg(BoardMessage('get board'))).__str__())
            print(response)
        elif game.lower()[0] == 'm':
            print(MessageCreator.create_message(client.send_msg(StartMessage('MoreOrLess'))).__str__())
            move = input()
            while not move.isdigit():
                move = input()
            response = MessageCreator.create_message(client.send_msg(MoveMessage(move)))
            while not isinstance(response, EndMessage):
                print(response.__str__())
                move = input()
                while not move.isdigit():
                    move = input()
                response = MessageCreator.create_message(client.send_msg(MoveMessage(move)))
            print(MessageCreator.create_message(client.send_msg(BoardMessage('get board'))).__str__())
            print(response)
        elif game.lower()[0] == 'q':
            break
