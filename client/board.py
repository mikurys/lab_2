class Board:

    def __init__(self, nick="Player"):
        self.board = 3*"+---+---+---+\n| {} | {} | {} |\n"+"+---+---+---+"
        self.nick = nick

    def display(self, board, number=20):
        self.clear_screen(number)
        print(self.board.format(*board))
        print("Your move. Choose number from 0 to 8.")

    @staticmethod
    def clear_screen(number):
        for i in range(number):
            print()

if __name__ == "__main__":
    board = Board()
    board.display([x for x in '012345678'])
